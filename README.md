Get started:
--------
1. Download/clone the repo
2. cd into 'Wumdrop' dir
3. run $python app.py in the terminal
4. open any browser and go to: http://localhost:5000

How it works
----------
The product page has a list of products which can be sorted by name or price. You can add these products to the order basket.

Use google maps to select a drop-off location. Then specify the address further by filling in line 1 of the drop-off address.
Fill in the rest of the details and click on 'Place delivery'.

You get then get a fare estimate and a waybill number. 
You can get further info about the delivery by clicking on 'Get more info..'.